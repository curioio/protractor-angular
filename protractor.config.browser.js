var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
const androidPixel2XLCapability = {
    browserName: '',
    autoWebview: true,
    autoWebviewTimeout: 20000,
    platformName: 'Android',
    deviceName: 'any',
   // app: '/Users/${user}/dev/curio-app-am-2/platforms/android/app/build/outputs/debug/app-debug.apk',
   app: '/Users/santtu/dev/curio-app-am-2/platforms/android/app/build/outputs/apk/debug/app-debug.apk',
    'app-package': 'io.curio',
    'app-activity': 'MainActivity',
    autoAcceptAlerts: 'true',
    autoGrantPermissions: 'true',
    newCommandTimeout: 300000
  };

  const chromeCapability = {
    'browserName': 'chrome',
        'chromeOptions': {                
            args: ['--disable-web-security']
        } 
  };

  const firefoxCapability = {
    'browserName': 'firefox'
  };

exports.config = {  
    allScriptsTimeout: 31000,
   // restartBrowserBetweenTests:true,
    capabilities:firefoxCapability,//firefoxCapability/chromeCapability
    directConnect: true,
    baseUrl: 'https:///play.curio.io',//'http://play-dev.curio.io','http://localhost:8100',
    specs: [
        'tests/ionic/*.tests.js'
    ],
    jasmineNodeOpts: {
        isVerbose: true,
    },
    onPrepare: function () {
    
    //    browser.driver.manage().window().setSize(360, 600);//??!!
    
        jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
            savePath: './test-reports/',//+ new Date().toISOString(),
            takeScreenshotsOnlyOnFailures: false
          }))
    }
};