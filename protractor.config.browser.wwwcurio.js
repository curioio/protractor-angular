var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

  const firefoxCapability = {
    'browserName': 'firefox'
  };

exports.config = {  
    allScriptsTimeout: 31000,
 
    capabilities:firefoxCapability,//firefoxCapability/chromeCapability
    directConnect: true,
    baseUrl: 'https://www.curio.io',
    SELENIUM_PROMISE_MANAGER: false,
    specs: [
        'tests/wwwcurio/*.tests.js'
    ],
    jasmineNodeOpts: {
        isVerbose: true,
    },
    onPrepare: function () {
        jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
            savePath: './test-reports/wwwcurio/',//+ new Date().toISOString(),
            takeScreenshotsOnlyOnFailures: false
          }))
    }
};