describe('Log-in', function(){  
    var username, password, signupButton,getStartedButton;
    console.log("runtest:Log-in");
    browser.waitForAngularEnabled(false);
  
  // await browser.driver.sleep(5000);
    beforeEach(async function() {
       
    
        await browser.executeScript('return window.sessionStorage.clear();');
       await browser.executeScript('return window.localStorage.clear();'); 
    });

    afterEach(async function() {
       
       await browser.restart(); 
    });

    var navigateFromHomePage = (async function() {
        browser.get('/');
        await browser.driver.sleep(5000);

        var EC = protractor.ExpectedConditions;
        var link = $('.log-in');
        var button = $('#get-started-button');
        var isClickable = EC.elementToBeClickable(button);
        var start = Date.now();
        console.log("beforeEach.isclickable.?:" + (Date.now()-start));
        await browser.wait(isClickable, 5000);
        console.log("beforeEach.isclickable.yes:" + (Date.now()-start));
        link.click();
        await browser.driver.sleep(2000);
       
    });
/*
    it('Should navigate to login page from landing page with all the elements visible',async function() {
       
         
     
         navigateFromHomePage();
        
          var emailInput =  element(by.css('#email input'));
        
          expect( emailInput.isPresent()).toBe(true);
  
          var passwordInput =  element(by.css('#password input'));
         
          expect( passwordInput.isPresent()).toBe(true);
          var loginButton =  element(by.css('.loginButton'));
          expect( loginButton.isPresent()).toBe(true);
      });
   */
/*
    xit('Login page handles errors',async function() {
       
          console.log("Login page handles errors");
          await navigateFromHomePage();
          await browser.driver.sleep(2000);
          var loginButton =  element(by.css('.loginButton'));
       
        
          loginButton.click();
          await browser.driver.sleep(100);
          var emailErrText = element(by.css("#email-error-text"));
          expect (emailErrText.isPresent()).toBe(true);
          element(by.css('#email input')).sendKeys('curio-e2e@curio.io');
          loginButton.click();
          await browser.driver.sleep(200);
          var passwordErrText = element(by.css("#password-error-text"));
          expect (passwordErrText.isPresent()).toBe(true);
          element(by.css('#password input')).sendKeys('e2e2e2');
          await browser.driver.sleep(6000);
          loginButton.click();
          await browser.driver.sleep(6000);
          var popupError = element(by.css(".alert-wrapper .alert-message"));
          expect (popupError.isPresent()).toBe(true);
        
      });
      */

      it('User can login',async function() {
       
        console.log("'User can login");
        await navigateFromHomePage();
        await browser.driver.sleep(2000);
        var loginButton =  element(by.css('.loginButton'));
        expect (loginButton.isPresent()).toBe(true);
        element(by.css('#email input')).sendKeys('curio-e2e@curio.io');
        element(by.css('#password input')).sendKeys('e2ee2e');
        await browser.driver.sleep(200);
        loginButton.click();
       
        await browser.driver.sleep(15000); 
        var trackTitle =  element(by.css('.track-title'));
        expect (trackTitle.isPresent()).toBe(true);

        var playlistContainer =  element(by.css('.playlist-container'));
       
        expect (playlistContainer.isPresent()).toBe(true);
        var productContainer =  element(by.css('.product-container'));
        expect (productContainer.isPresent()).toBe(true);
    });

 
     
   // it('should display a error message for invalid email', function() {
        // TODO: test unsuccessful login
  //  });

   // it('should display a error message for invalid password', function() {
        // TODO: test unsuccessful login
   // });

  //  it('should display a popup message for existing user account', function() {
        // TODO: test unsuccessful login
  //  });

});