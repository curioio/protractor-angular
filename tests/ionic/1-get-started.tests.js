describe('Landing page loaded', function(){  
    var  getStartedButton;
    browser.waitForAngularEnabled(false);
   
    console.log("Landing page loaded");
    beforeEach(function() {
        browser.get('/');
       
    
     
    });

    it('should show get Started button visible',async function() {
        console.log("test.should show get Started button visible");
        console.log(" ispresent:" +await element(by.id('get-started-button')).isPresent());
     
        var EC = protractor.ExpectedConditions;
        var button = $('#get-started-button');
        var isClickable = EC.elementToBeClickable(button);
        var start = Date.now();
        await browser.wait(isClickable, 10000);
        expect(await element(by.id('get-started-button')).isPresent()).toBe(true);
    
    })
});