describe('Landing page', function(){  
   
    browser.waitForAngularEnabled(false);
   
    console.log("Landing page loaded - test running");
    beforeEach(async function() {
        browser.get('/');
        var EC = protractor.ExpectedConditions;
        var button = $('#homeSliderGetStarted');
        var isClickable = EC.elementToBeClickable(button);
    
        await browser.wait(isClickable, 10000);
    });

    it('should show get Started button visible',async function() {
        console.log("test.should show get Started button visible");
        expect(await element(by.id('homeSliderGetStarted')).isPresent()).toBe(true);
      
    });

    it('should be able to navigate to login page',async function() {
    
        element(by.id('homeSliderGetStarted')).click();
     
        await browser.driver.sleep(2000);
        expect(await element(by.id('UserSignUp')).isPresent()).toBe(true);
    });

});