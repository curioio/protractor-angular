describe('Log-in', function(){  
    var username, password, signupButton,getStartedButton;
    console.log("runtest:Log-in");
    browser.waitForAngularEnabled(false);
  
  // await browser.driver.sleep(5000);
    beforeEach(async function() {
       
    
        await browser.executeScript('return window.sessionStorage.clear();');
       await browser.executeScript('return window.localStorage.clear();'); 
    });

    afterEach(async function() {
       
       await browser.restart(); 
    });

    var navigateFromHomePage = (async function() {
        var EC = protractor.ExpectedConditions;
        var button = $('#get-started-button');
        var isClickable = EC.elementToBeClickable(button);
      
        await browser.wait(isClickable, 10000);
       
    
        var link = $('.log-in');
      
        link.click();
       
       
    });

      it('User can login',async function() {
       
        console.log("'User can login");
        await navigateFromHomePage();
        await browser.driver.sleep(5000);
        var loginButton =  element(by.css('.loginButton'));
        expect (await loginButton.isPresent()).toBe(true);
        await element(by.css('#email input')).sendKeys('curio+logintestaccount@curio.io');
        await element(by.css('#password input')).sendKeys('logintest');
        await element(by.css('#password input')).click();
        await browser.driver.sleep(200);
        loginButton.click();
       
        await browser.driver.sleep(15000); 
        var trackTitle =  element(by.css('.track-title'));
        expect (await trackTitle.isPresent()).toBe(true);

        var playlistContainer =  element(by.css('.playlist-container'));
       
        expect (await playlistContainer.isPresent()).toBe(true);
        var productContainer =  element(by.css('.product-container'));
        expect (await productContainer.isPresent()).toBe(true);
    });

 
     
   // it('should display a error message for invalid email', function() {
        // TODO: test unsuccessful login
  //  });

   // it('should display a error message for invalid password', function() {
        // TODO: test unsuccessful login
   // });

  //  it('should display a popup message for existing user account', function() {
        // TODO: test unsuccessful login
  //  });

});