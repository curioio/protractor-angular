


describe('Landing page tests', function(){  
    var  getStartedButton;
  
    browser.waitForAngularEnabled(false);
    console.log("Landing page tests");
   
    beforeEach( function() {
        console.log("beforeEach" );
        
    });

    afterEach(function() {
        console.log("afterEach:");
     
    });

    afterAll(function() {
        console.log("afterAll");
       
    });
  
    it('should show get Started button visible',async function() {
        var start = Date.now();
        console.log("test.should show get Started button visible");
       // console.log(" ispresent:" +await element(by.id('get-started-button')).isPresent());
     
        var EC = protractor.ExpectedConditions;
        var button = $('#get-started-button');
        var isClickable = EC.elementToBeClickable(button);
      
        await browser.wait(isClickable, 10000);

        
        console.log("waited-isclickable:" + (Date.now()-start));
         
        expect(await element(by.id('get-started-button')).isDisplayed()).toBe(true);

         
    });

   

  
});

