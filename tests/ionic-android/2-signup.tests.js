

describe('Sign-up ', function(){  
    var username, password, signupButton,getStartedButton;
    console.log("runtest:Signup");
    browser.waitForAngularEnabled(false);
    beforeEach( function() {
       browser.refresh();
       
    });
    afterEach( function() {
        try{
            console.log("afterEach");
       //     browser.executeScript('window.sessionStorage.clear();');
         //   browser.executeScript('window.localStorage.clear();');
        }
        catch(err)
        {
            console.log("afterEach.err",err);
        }
      
       
    });
   
    var navigateFromHomePage = (async function() {
        try{
            var EC = protractor.ExpectedConditions;
            var button = $('#get-started-button');
            var isClickable = EC.elementToBeClickable(button);
          
            await browser.wait(isClickable, 10000);
            element(by.id('get-started-button')).click();
          
        }
        catch(err)
        {
            console.log("navigateFromHomePage.err",err);
        }
     
    });

    it('Should navigate to signup page from landing page with all the elements visible',async function() {
        //  browser.waitForAngular();
          console.log("test.Should navigate to signup page from landing page with all the elements visible");
        //  var getStartedButton = element(by.id('get-started-button'));
        //  console.log("getStartedButton.isPresent:"+ getStartedButton.isPresent());
       //   getStartedButton.click();
       await navigateFromHomePage();
          await browser.driver.sleep(2000);
          var submitButton =  element(by.css('.submitButton'));
         
          expect(await submitButton.isPresent()).toBe(true);
  
          var emailInput =  element(by.css('#email input'));
         
          expect(await  emailInput.isPresent()).toBe(true);;
  
          var passwordInput =  element(by.css('#password input'));
         
          expect(await  passwordInput.isPresent()).toBe(true);
      });
  

    it('Signup page handles errors',async function() {
       
          console.log("test.Signup page handles errors");
          await navigateFromHomePage();
          await browser.driver.sleep(2000);
          var submitButton =  element(by.css('.submitButton'));
         
        
          submitButton.click();
          await browser.driver.sleep(100);
          var emailErrText = element(by.css("#email-error-text"));
          expect (await emailErrText.isDisplayed()).toBe(true);
          browser.takeScreenshot();
          element(by.css('#email input')).sendKeys('santtu@curio.io');
          submitButton.click();
          await browser.driver.sleep(200);
          var passwordErrText = element(by.css("#password-error-text"));
          expect (await passwordErrText.isDisplayed()).toBe(true);
          browser.takeScreenshot();
          element(by.css('#password input')).sendKeys('Password123');
          await browser.driver.sleep(6000);
          submitButton.click();
          await browser.driver.sleep(6000);
          var popupError = element(by.css(".alert-wrapper .alert-message"));
          expect (await popupError.isDisplayed()).toBe(true);
          browser.takeScreenshot();
        
      });

      it('User can sign up with email address and password',async function() {
       
        console.log("User can sign up with email address and password");
        await navigateFromHomePage();
        await browser.driver.sleep(9000);
        var submitButton =  element(by.css('.submitButton'));
        console.log("submitButton.isPresent:"+ await submitButton.isPresent());

        element(by.css('#password input')).sendKeys('e2ee2e');
        element(by.css('#email input')).sendKeys('ionic-e2e+'+ Date.now() + '@curio.io');
        await browser.driver.sleep(500);
        submitButton.click();
        await browser.driver.sleep(9000);
        var gotoAppButton =  element(by.css('#skip-link'));
        expect (await gotoAppButton.isPresent()).toBe(true);
        gotoAppButton.click();
        await browser.driver.sleep(7000); 
        var trackTitle =  element(by.css('.track-title'));
        expect (await trackTitle.isPresent()).toBe(true);

        var playlistContainer =  element(by.css('.playlist-container'));
       
        expect (await playlistContainer.isPresent()).toBe(true);
      
    });

    it('Subscription popup visible and opens by clicking subs banner',async function() {
        console.log("Subscription popup visible and opens by clicking subs banner");
        await browser.driver.sleep(15000); 
        var productContainer =  element(by.css('.product-container'));
        console.log("Track and subscription4");
        expect (await productContainer.isPresent()).toBe(true);
        element(by.css('.modalDown')).click();
        await browser.driver.sleep(1000); 
        productContainer =  element(by.css('.product-container'));
        expect (await productContainer.isPresent()).toBe(false);
       
        element(by.css('.subscription-nag')).click(); 
        await browser.driver.sleep(1000); 
        productContainer =  element(by.css('.product-container'));
        expect (await productContainer.isPresent()).toBe(true);
    });
     
  

});