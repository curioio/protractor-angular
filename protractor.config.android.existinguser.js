
var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');//require('protractor-jasmine2-screenshot-reporter');
var reporter = new Jasmine2HtmlReporter({
  savePath: './test-reports/'+ new Date().toISOString(),
  takeScreenshotsOnlyOnFailures: false,
  takeScreenshots:true

});

  const androidCapability = {
    "autoWebview": true,
    "autoWebviewTimeout": 60000,
    "platformName": "Android",
    "browserName": "",
    "deviceName": "any",
    "appPackage":"io.curio",
    "app": "/Users/santtu/dev/curio-app-am-2/platforms/android/app/build/outputs/apk/debug/app-debug.apk",    
    "autoGrantPermissions": true,
    "newCommandTimeout": 600000,
    "uiautomator2ServerLaunchTimeout":600000,
    "automationName":"Appium",//"UiAutomator2",
    "noReset":false
  };

exports.config = {  
    allScriptsTimeout: 31000,
   
    baseUrl:"http://localhost:8000",// 'http://10.0.2.2:8000',
    seleniumAddress: 'http://localhost:4723/wd/hub',

   capabilities:androidCapability,
   chromedriverExecutableDir:'appium-chromedriver-automatic-downloads/',
    specs: [
     'tests/ionic-android/3-login.tests.js',
    ],
    jasmineNodeOpts: {
      showColors: true,
        isVerbose: true,
    },
   
    onPrepare: async function () {
        try{
          browser.waitForAngularEnabled(false);
          jasmine.getEnv().addReporter(reporter);
    
         
        }
        catch(err)
        {
            console.log("onPrepare.err",err);
        }
    },
   
    resultJsonOutputFile:"testres.json",
 
    SELENIUM_PROMISE_MANAGER: false,
 
};